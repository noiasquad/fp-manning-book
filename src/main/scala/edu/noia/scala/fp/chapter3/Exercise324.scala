package edu.noia.scala.fp.chapter3

// implement hasSubsequence for checking whether a List contains another List as a subsequence
object Exercise324 {

  @annotation.tailrec
  def startWith[A](a: List[A], b: List[A]): Boolean = (a, b) match {
    case (Nil, _) => b == Nil
    case (x :: xs, y :: ys) if x == y => startWith(xs, ys)
    case _ => b == Nil
  }

  @annotation.tailrec
  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = sup match {
    case Nil => sub == Nil
    case _ if startWith(sup, sub) => true
    case _::xs => hasSubsequence(xs, sub)
  }
}