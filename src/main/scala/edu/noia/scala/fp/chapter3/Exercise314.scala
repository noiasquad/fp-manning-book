package edu.noia.scala.fp.chapter3

// Implement append in terms of either foldLeft or foldRight
// The foldLeft variant requires a reverse, this one expressed in terms of foldLeft
object Exercise314 {

  import Exercise38._
  import Exercise310._
  import Exercise312._

  def append[A](a1: List[A], a2: List[A]): List[A] = appendL(a1, a2)

  def appendR[A](a1: List[A], a2: List[A]): List[A] = foldRight(a1, a2)((a, b) => a :: b)

  def appendL[A](a1: List[A], a2: List[A]): List[A] = foldLeft(reverse(a1), a2)((b, a) => a :: b)
}
