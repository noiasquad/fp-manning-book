package edu.noia.scala.fp.chapter3

// Write a function that concatenates a list of lists into a single list.
// Its runtime should be linear in the total length of all lists.
object Exercise315 {

  import Exercise310._
  import Exercise314._

  def concat[A](as: List[List[A]]): List[A] = foldLeft(as, Nil: List[A])(append)
}
