package edu.noia.scala.fp.chapter3

// Write a function that transforms a list of integers by adding 1 to each element.
object Exercise316 {

  import Exercise310._
  import Exercise312._
  import Exercise38._

  def incL(as: List[Int], n: Int): List[Int] = foldLeft(reverse(as), Nil: List[Int])((b, a) => (a + n) :: b)

  def incR(as: List[Int], n: Int): List[Int] = foldRight(as, Nil: List[Int])((a, b) => (a + n) :: b)
}
