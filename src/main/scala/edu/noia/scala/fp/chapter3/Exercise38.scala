package edu.noia.scala.fp.chapter3

// What do you think this says about the relationship between foldRight and the data constructors of List?
object Exercise38 {

  val Cons = ::

  def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B =
    as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }
}