package edu.noia.scala.fp.chapter3

// Generalize tail to the function drop, which removes the first n elements from a list.
object Exercise34 {

  // Note that this function takes time proportional only to the number of elements being
  // dropped—we don’t need to make a copy of the entire List.
  def drop[A](l: List[A], n: Int): List[A] = {
    if (n > 0) l match {
      case _ :: xs => drop(xs, n-1)
      case Nil => Nil
    } else l
  }
}
