package edu.noia.scala.fp.chapter3

object Exercise33 {

  def setHead[A](l: List[A], element: A): List[A] = l match {
    case Nil => List(element)
    case _ :: xs =>  element :: xs
  }
}
