package edu.noia.scala.fp.chapter3

// Write a function that turns each value in a List[Double] into a String.
object Exercise317 {

  import Exercise310._
  import Exercise312._
  import Exercise38._

  def toStringL(as: List[Double]): List[String] = foldLeft(reverse(as), Nil: List[String])((b, a) => a.toString :: b)

  def toStringR(as: List[Double]): List[String] = foldRight(as, Nil: List[String])((a, b) => a.toString :: b)
}
