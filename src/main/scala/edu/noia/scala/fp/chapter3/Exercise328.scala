package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise325.{Branch, Leaf, Tree}

// Write a function map, analogous to the method of the same name on List, that modifies
// each element in a tree with a given function.
object Exercise328 {

  def map[A, B](tree: Tree[A])(f: A => B): Tree[B] = tree match {
    case Leaf(v) => Leaf(f(v))
    case Branch(l, r) => Branch(map(l)(f), map(r)(f))
  }
}
