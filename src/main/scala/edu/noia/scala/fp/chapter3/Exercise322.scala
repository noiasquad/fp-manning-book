package edu.noia.scala.fp.chapter3

// Write a function that accepts two lists and constructs a new list by adding corresponding elements.
object Exercise322 {

  val Cons = ::

  def zipInt(a: List[Int], b: List[Int]): List[Int] = (a, b) match {
    case (Nil, _) => b
    case (_, Nil) => a
    case (Cons(x, xs), Cons(y, ys)) => Cons(x + y, zipInt(xs, ys))
  }

}