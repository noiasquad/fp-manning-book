package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise325._

// Write a function maximum that returns the maximum element in a Tree[Int].
object Exercise326 {

  def max[A](tree: Tree[A])(implicit ordering: Ordering[A]): A = tree match {
    case Leaf(v) => v
    case Branch(l, r) => Seq(max(l), max(r)).max
  }
}
