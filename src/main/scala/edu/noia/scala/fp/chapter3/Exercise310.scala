package edu.noia.scala.fp.chapter3

// write another general list-recursion function, foldLeft, that is tail-recursive
object Exercise310 {

  def foldLeft[A, B](as: List[A], z: B)(f: (B, A) => B): B = {
    @annotation.tailrec
    def g(l: List[A], acc: B): B =
      l match {
        case x :: xs => g(xs, f(acc, x))
        case _ => acc
      }
    g(as, z)
  }
}
