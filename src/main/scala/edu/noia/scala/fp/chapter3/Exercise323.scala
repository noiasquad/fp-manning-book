package edu.noia.scala.fp.chapter3

// Write a function that accepts two lists and constructs a new list by adding corresponding elements.
object Exercise323 {

  val Cons = ::

  def zip[A](a: List[A], b: List[A])(f: (A, A) => A): List[A] = (a, b) match {
    case (Nil, _) => a
    case (_, Nil) => b
    case (Cons(x, xs), Cons(y, ys)) => Cons(f(x, y), zip(xs, ys)(f))
  }

}