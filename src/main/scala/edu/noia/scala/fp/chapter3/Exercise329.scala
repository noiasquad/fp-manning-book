package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise325.{Branch, Leaf, Tree}

// Generalize size, maximum, depth, and map, writing a new function fold that abstracts
// over their similarities. Reimplement them in terms of this more general function.
object Exercise329 {

  // `fold` receives a "handler" for each of the data constructors of the type, and recursively
  //  accumulates some value using these handlers.
  def fold[A, B](tree: Tree[A])(f: A => B)(g: (B, B) => B): B = tree match {
    case Leaf(v) => f(v)
    case Branch(l, r) =>
      val lv = fold(l)(f)(g)
      val rv = fold(r)(f)(g)
      g(lv, rv)
  }
}
