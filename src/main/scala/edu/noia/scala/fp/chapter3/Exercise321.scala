package edu.noia.scala.fp.chapter3

// Use flatMap to implement filter.
object Exercise321 {

  import Exercise320._

  def filter[A](as: List[A])(f: A => Boolean): List[A] = flatMapL(as)(a => if (f(a)) List(a) else Nil)
}