package edu.noia.scala.fp.chapter3

// Can you write foldLeft in terms of foldRight? How about the other way around?
// The operation has to be commutative when reducing: +, - are ok, append is not ok
object Exercise313 {

  import Exercise38._
  import Exercise310._
  import Exercise312._

  // works only for reducing with a commutative operator
  def foldLeftR[A, B](as: List[A], z: B)(f: (B, A) => B): B = foldRight(as, z)((a,b) => f(b,a))

  def foldRightL[A, B](as: List[A], z: B)(f: (A, B) => B): B = foldLeft(reverse(as), z)((b,a) => f(a,b))
}
