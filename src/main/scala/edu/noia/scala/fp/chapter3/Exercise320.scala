package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise312.reverse

// Write a function flatMap that works like map except that the function given will return
// a list instead of a single result, and that list should be inserted into the final resulting list.
object Exercise320 {

  import Exercise310._
  import Exercise314._
  import Exercise38._

  def flatMap[A, B](as: List[A])(f: A => List[B]): List[B] = flatMapL(as)(f)

  def flatMapL[A, B](as: List[A])(f: A => List[B]): List[B] =
    foldLeft(reverse(as), Nil: List[B])((b, a) => append(f(a), b))

  def flatMapR[A, B](as: List[A])(f: A => List[B]): List[B] =
    foldRight(as, Nil: List[B])((a, b) => append(f(a), b))
}