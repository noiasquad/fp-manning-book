package edu.noia.scala.fp.chapter3

// Write a function filter that removes elements from a list unless they satisfy a given predicate.
// Use it to remove all odd numbers from a List[Int].
object Exercise319 {

  import Exercise310._
  import Exercise312._
  import Exercise38._

  def filter[A](as: List[A])(f: A => Boolean): List[A] = filterL(as)(f)

  def filterL[A](as: List[A])(f: A => Boolean): List[A] =
    foldLeft(reverse(as), Nil: List[A])((b, a) => if (f(a)) a :: b else b)

  def filterR[A](as: List[A])(f: A => Boolean): List[A] =
    foldRight(as, Nil: List[A])((a, b) => if (f(a)) a :: b else b)
}
