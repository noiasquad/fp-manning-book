package edu.noia.scala.fp.chapter3

object Exercise32 {

  def tail[A](l: List[A]): List[A] = l match {
    case Nil => Nil
    case _ :: xs => xs
  }
}
