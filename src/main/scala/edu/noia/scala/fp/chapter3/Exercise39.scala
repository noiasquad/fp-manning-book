package edu.noia.scala.fp.chapter3

// Compute the length of a list using foldRight.
object Exercise39 {

  import Exercise38._

  def length[A](as: List[A]): Int = foldRight(as, 0)((_, b) => b + 1)
}
