package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise310.foldLeft

// Write a function that returns the reverse of a list
// See if you can write it using a fold
object Exercise312 {

  def reverse[A](as: List[A]): List[A] = foldLeft(as, Nil: List[A])((b, a) => a :: b)
}
