package edu.noia.scala.fp.chapter3

// Write a function map that generalizes modifying each element in a list while maintaining the structure of the list.
object Exercise318 {

  import Exercise310._
  import Exercise312._
  import Exercise38._

  def map[A, B](as: List[A])(f: A => B): List[B] = mapL(as)(f)

  def mapL[A, B](as: List[A])(f: A => B): List[B] = foldLeft(reverse(as), Nil: List[B])((b, a) => f(a) :: b)

  def mapR[A, B](as: List[A])(f: A => B): List[B] = foldRight(as, Nil: List[B])((a, b) => f(a) :: b)
}
