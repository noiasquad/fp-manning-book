package edu.noia.scala.fp.chapter3

// Implement dropWhile
object Exercise35 {

  // removes elements from the List prefix as long as they match a predicate
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] =
    l match {
      case x :: xs if f(x) => dropWhile(xs, f)
      case _ => l
    }

  def dropWhileC[A](l: List[A])(f: A => Boolean): List[A] =
    l match {
      case x :: xs if f(x) => dropWhileC(xs)(f)
      case _ => l
    }
}
