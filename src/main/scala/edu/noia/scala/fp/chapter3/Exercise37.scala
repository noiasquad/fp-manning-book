package edu.noia.scala.fp.chapter3

// Can product, implemented using foldRight, immediately halt the recursion and
// return 0.0 if it encounters a 0.0? Why or why not?
// Consider how any short-circuiting might work if you call foldRight with a large list.
object Exercise37 {

  val Cons = ::

  def product1(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product1(xs)
  }

  // without the “short-circuiting” logic of checking for 0.0
  def product2(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(x, xs) => x * product2(xs)
  }

  def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B =
    as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }

  def product3(ds: List[Double]): Double = foldRight(ds, 1.0)(_ * _)

  def product(ds: List[Double]): Double = ???
}
