package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise325._

// Write a function depth that returns the maximum path length from the root of a tree to any leaf.
object Exercise327 {

  def depth[A](tree: Tree[A]): Int = tree match {
      case Leaf(_) => 1
      case Branch(l, r) => 1 + Seq(depth(l), depth(r)).max
  }
}
