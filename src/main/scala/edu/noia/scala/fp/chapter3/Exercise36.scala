package edu.noia.scala.fp.chapter3

// Implement a function, init, that returns a List consisting of all but the last element of a List.
// Why can’t this function be implemented in constant time like tail?
object Exercise36 {

  // returns a List consisting of all but the last element of a List
  def init[A](l: List[A]): List[A] = {
    @annotation.tailrec
    def loop(l: List[A], acc: List[A]): List[A] =
      l match {
        case x :: xs if xs.nonEmpty => loop(xs, x :: acc)
        case _ => acc.reverse
      }

    loop(l, Nil)
  }

  // returns a List consisting of all but the last element of a List
  def initR[A](l: List[A]): List[A] =
    l match {
      case x :: xs => if (xs.nonEmpty) x :: initR(xs) else Nil
      case _ => l
    }
}
