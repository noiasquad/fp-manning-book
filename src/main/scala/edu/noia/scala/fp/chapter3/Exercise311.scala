package edu.noia.scala.fp.chapter3

// Write sum, product, and a function to compute the length of a list using foldLeft.
object Exercise311 {

  import Exercise310._

  def length[A](as: List[A]): Int = foldLeft(as, 0)((a, _) => a + 1)

  def sum(as: List[Int]): Int = foldLeft(as, 0)(_ + _)

  def product(as: List[Int]): Int = if (as.nonEmpty) foldLeft(as, 1)(_ * _) else 0
}
