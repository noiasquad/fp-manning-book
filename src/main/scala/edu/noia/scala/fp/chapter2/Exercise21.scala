package edu.noia.scala.fp.chapter2

object Exercise21 {

  def fibonacciR(n: Int): Int = {
    if (n == 0) 0
    else if (n == 1) 1
    else fibonacciR(n-1) + fibonacciR(n-2)
  }

  def fibonacci(n: Int): Int = {
    if (n == 0) 0
    else if (n == 1) 1
    else {
      @annotation.tailrec
      def loop(x: Int, nb1: Int, nb2: Int): Int = {
        if (x == n) nb2
        else if (x == 0) loop(1, 0, 1)
        else loop(x + 1, nb2, nb1 + nb2)
      }
      loop(1, 0, 1)
    }
  }

  def factorialR(n: Int): Int = {
    if (n == 0) 1 else n * factorialR(n - 1)
  }

  def factorial(n: Int): Int = {
    @annotation.tailrec
    def loop(x: Int, acc: Int): Int = {
      if (x == 0) acc
      else loop(x - 1, acc * x)
    }
    loop(n, 1)
  }

}
