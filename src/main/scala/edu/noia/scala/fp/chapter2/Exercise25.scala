package edu.noia.scala.fp.chapter2

// function composition, which feeds the output of one function to the input of another function.
object Exercise25 {

  // higher-order function that composes two functions
  def compose[A, B, C](f: B => C, g: A => B): A => C =
    a => f(g(a))

  // higher-order function that composes two functions
  def composeDefault1[A, B, C](f: B => C, g: A => B): A => C =
    f compose g

  // higher-order function that composes two functions
  def composeDefault2[A, B, C](f: B => C, g: A => B): A => C =
    g andThen f
}
