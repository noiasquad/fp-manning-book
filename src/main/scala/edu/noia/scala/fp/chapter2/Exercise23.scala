package edu.noia.scala.fp.chapter2

// currying,9 which converts a function f of two arguments into a function of one argument that partially applies f.
object Exercise23 {

  // => associates to the right
  def curry[A, B, C](f: (A, B) => C): A => B => C =
    a => b => f(a,b)
}
