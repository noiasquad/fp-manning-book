package edu.noia.scala.fp.chapter4

import edu.noia.scala.fp.chapter3.Exercise38._
import edu.noia.scala.fp.chapter4.Exercise41.{None, Option, Some}

/*
Write a function sequence that combines a list of Options into one Option containing
a list of all the Some values in the original list.
*/
object Exercise44 {

  def sequence[A](a: List[Option[A]]): Option[List[A]] = {

    def combine(x: Option[A], y: Option[List[A]]): Option[List[A]] = (x,y) match {
      case (None, _) => None
      case (_, None) => None
      case (Some(h), Some(ys)) => Some(Cons(h, ys))
    }

    foldRight(a, Some(Nil): Option[List[A]])(combine)
  }
}