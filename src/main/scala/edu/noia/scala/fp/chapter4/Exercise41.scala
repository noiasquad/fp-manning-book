package edu.noia.scala.fp.chapter4

object Exercise41 {

  trait Option[+A] {

    def map[B](f: A => B): Option[B] = this match {
      case Some(a) => Some(f(a))
      case _ => None
    }

    def flatMap[B](f: A => Option[B]): Option[B] = map(f) getOrElse None

    def getOrElse[B >: A](default: => B): B = this match {
      case Some(a) => a
      case _ => default
    }

    def orElse[B >: A](ob: => Option[B]): Option[B] = this match {
      case Some(_) => this
      case _ => ob
    }

    def filter(f: A => Boolean): Option[A] = this match {
      case Some(a) if f(a) => this
      case _ => None
    }
  }

  case class Some[+A](a: A) extends Option[A]

  case object None extends Option[Nothing]

}