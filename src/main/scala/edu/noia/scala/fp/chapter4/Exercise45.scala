package edu.noia.scala.fp.chapter4

import edu.noia.scala.fp.chapter3.Exercise38.{Cons, foldRight}
import edu.noia.scala.fp.chapter4.Exercise41.{None, Option, Some}
import edu.noia.scala.fp.chapter4.Exercise44._

/*
Write a function sequence that combines a list of Options into one Option containing
a list of all the Some values in the original list.
*/
object Exercise45 {

  def Try[A](a: => A): Option[A] =
    try Some(a)
    catch {
      case _: Exception => None
    }

  def parseInts(a: List[String]): Option[List[Int]] = sequence(a map (i => Try(i.toInt)))

  def parseInts1(a: List[String]): Option[List[Int]] = traverse(a)(i => Try(i.toInt))

  def parseInts2(a: List[String]): Option[List[Int]] = traverse2(a)(i => Try(i.toInt))

  def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = sequence(a map f)

  def traverse2[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = {

    def combine(x: A, y: Option[List[B]]): Option[List[B]] = (f(x), y) match {
      case (None, _) => None
      case (_, None) => None
      case (Some(h), Some(ys)) => Some(Cons(h, ys))
    }

    foldRight(a, Some(Nil): Option[List[B]])(combine)
  }
}