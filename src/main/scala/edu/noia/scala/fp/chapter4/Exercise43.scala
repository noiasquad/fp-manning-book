package edu.noia.scala.fp.chapter4

import edu.noia.scala.fp.chapter4.Exercise41.{None, Option, Some}

/*
Write a generic function map2 that combines two Option values using a binary function.
If either Option value is None, then the return value is too.
*/
object Exercise43 {

  def map2[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = (a, b) match {
    case (None, _) => None
    case (_, None) => None
    case (Some(x), Some(y)) => Some(f(x, y))
  }

  def map2_1[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] =
    a flatMap (aa => b map (bb => f(aa, bb)))

  def map2_2[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] =
    for {
      aa <- a
      bb <- b
    } yield f(aa, bb)

}
