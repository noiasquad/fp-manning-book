package edu.noia.scala.fp.chapter2

import org.scalatest.{FlatSpec, Matchers}

class Exercise21Test extends FlatSpec with Matchers {
  
  import Exercise21._

  "Fibonacci recursive function" should "return the nth Fibonacci number" in {
    fibonacciR(0) should be(0)
    fibonacciR(1) should be(1)
    fibonacciR(2) should be(1)
    fibonacciR(3) should be(2)
    fibonacciR(4) should be(3)
    fibonacciR(5) should be(5)
    fibonacciR(6) should be(8)
    fibonacciR(7) should be(13)
    fibonacciR(8) should be(21)
  }

  "Fibonacci tail recursive function" should "return the nth Fibonacci number" in {
    fibonacci(0) should be(0)
    fibonacci(1) should be(1)
    fibonacci(2) should be(1)
    fibonacci(3) should be(2)
    fibonacci(4) should be(3)
    fibonacci(5) should be(5)
    fibonacci(6) should be(8)
    fibonacci(7) should be(13)
    fibonacci(8) should be(21)
  }

  "Factorial recursive function" should "calculate its nth number" in {
    factorialR(0) should be(1)
    factorialR(1) should be(1)
    factorialR(2) should be(2)
    factorialR(3) should be(6)
    factorialR(4) should be(24)
    factorialR(5) should be(120)
    factorialR(6) should be(720)
  }

  "Factorial tail recursive function" should "calculate its nth number" in {
    factorial(0) should be(1)
    factorial(1) should be(1)
    factorial(2) should be(2)
    factorial(3) should be(6)
    factorial(4) should be(24)
    factorial(5) should be(120)
    factorial(6) should be(720)
  }

}