package edu.noia.scala.fp.chapter2

import org.scalatest.{FlatSpec, Matchers}

class Exercise22Test extends FlatSpec with Matchers {
  
  import Exercise22._

  val asc = (a: Int, b: Int) => a <= b
  val desc = (a: Int, b: Int) => a >= b

  "Comparators" should "return true when ordered" in {
    asc(1, 2) should be(true)
    desc(2, 1) should be(true)
  }

  "Checker" should "return true when array is sorted" in {
    isSorted(new Array(0), asc) should be(true)
    isSorted(Array(3,2,1), desc) should be(true)
    isSorted(Array(1), asc) should be(true)
  }

  "Checker" should "return false when array is not sorted" in {
    isSorted(Array(1,2), desc) should be(false)
    isSorted(Array(2,1), asc) should be(false)
  }

}