package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise325.{Branch, Leaf}
import org.scalatest.{FlatSpec, Matchers}

class Exercise326Test extends FlatSpec with Matchers {

  "max" should "return the maximal value of a tree" in {
    Exercise326.max(Leaf(3)) should be(3)
    Exercise326.max(Branch(Leaf(3), Branch(Leaf(1), Leaf(5)))) should be(5)
  }

}