package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise325.{Branch, Leaf}
import org.scalatest.{FlatSpec, Matchers}

class Exercise327Test extends FlatSpec with Matchers {

  "depth" should "return the depth of a tree" in {
    Exercise327.depth(Leaf(3)) should be(1)
    Exercise327.depth(Branch(Leaf(3), Branch(Leaf(1), Leaf(5)))) should be(3)
    Exercise327.depth(Branch(Branch(Leaf(1), Leaf(5)), Branch(Leaf(1), Leaf(7)))) should be(3)
  }
}