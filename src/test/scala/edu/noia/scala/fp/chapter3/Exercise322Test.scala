package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise322Test extends FlatSpec with Matchers {

  import Exercise322._

  "zipInt" should "add the content of two lists pairwise" in {
    zipInt(List(1, 2, 3), List(4, 5, 6)) should be(List(5, 7, 9))
  }
}