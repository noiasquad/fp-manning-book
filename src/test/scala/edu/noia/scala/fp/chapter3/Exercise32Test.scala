package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise32Test extends FlatSpec with Matchers {

  import Exercise32._

  "Tail function" should "remove the tail element of a list and return the tail" in {
    tail(List(1,2,3)) should be(List(2,3))
    tail(Nil) should be(Nil)
  }
}