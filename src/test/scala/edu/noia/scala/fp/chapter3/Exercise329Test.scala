package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise325.{Branch, Leaf, Tree}
import org.scalatest.{FlatSpec, Matchers}

class Exercise329Test extends FlatSpec with Matchers {

  import Exercise329._

  "sizeWithFold" should "count the number of nodes and leaves in a tree" in {
    val f = (_: Int) => 1

    fold(Leaf(3))(f)(1 + _ + _) should be(1)
    fold(Branch(Leaf(3), Leaf(5)))(f)(1 + _ + _) should be(3)
    fold(Branch(Branch(Leaf(1), Leaf(5)), Leaf(3)))(f)(1 + _ + _) should be(5)
    fold(Branch(Leaf(3), Branch(Leaf(1), Leaf(5))))(f)(1 + _ + _) should be(5)
    fold(Branch(Branch(Leaf(1), Leaf(5)), Branch(Leaf(1), Leaf(7))))(f)(1 + _ + _) should be(7)
  }

  "maxWithFold" should "find the maximum value in a tree" in {
    val f = (a: Int) => a
    val g = (a: Int, b: Int) => a max b

    fold(Leaf(3))(f)(g) should be(3)
    fold(Branch(Leaf(3), Leaf(5)))(f)(g) should be(5)
    fold(Branch(Branch(Leaf(1), Leaf(5)), Leaf(3)))(f)(g) should be(5)
    fold(Branch(Leaf(3), Branch(Leaf(1), Leaf(5))))(f)(g) should be(5)
    fold(Branch(Branch(Leaf(1), Leaf(5)), Branch(Leaf(1), Leaf(7))))(f)(g) should be(7)
    fold(Branch(Branch(Leaf(1), Leaf(5)), Branch(Leaf(1), Leaf(3))))(f)(g) should be(5)
  }

  "depthWithFold" should "find the depth of a tree" in {
    val f = (_: Int) => 1
    val g = (a: Int, b: Int) => 1 + (a max b)

    fold(Leaf(3))(f)(g) should be(1)
    fold(Branch(Leaf(3), Leaf(5)))(f)(g) should be(2)
    fold(Branch(Branch(Leaf(1), Leaf(5)), Leaf(3)))(f)(g) should be(3)
    fold(Branch(Leaf(3), Branch(Leaf(1), Leaf(5))))(f)(g) should be(3)
    fold(Branch(Branch(Leaf(1), Leaf(5)), Branch(Leaf(1), Leaf(7))))(f)(g) should be(3)
  }

  /*
    When working with algebraic data types in Scala, it's somewhat
    common to define helper functions that simply call the corresponding data constructors but give the less specific
    result type:

    def leaf[A](a: A): Tree[A] = Leaf(a)
    def branch[A](l: Tree[A], r: Tree[A]): Tree[A] = Branch(l, r)
   */

  "mapWithFold" should "modify each element in a tree with a given function f" in {
    val f = (v: Int) => v + 1

    fold(Leaf(3))(a => Leaf(f(a)): Tree[Int])(Branch(_, _)) should be(Leaf(4))
    fold(Branch(Leaf(3), Leaf(5)))(a => Leaf(f(a)): Tree[Int])(Branch(_, _)) should be(Branch(Leaf(4), Leaf(6)))

    def leaf(a: Int): Tree[Int] = Leaf(f(a))
    def branch(l: Tree[Int], r: Tree[Int]): Tree[Int] = Branch(l, r)

    fold(Leaf(3))(leaf)(branch) should be(Leaf(4))
    fold(Branch(Leaf(3), Leaf(5)))(leaf)(branch) should be(Branch(Leaf(4), Leaf(6)))
  }
}