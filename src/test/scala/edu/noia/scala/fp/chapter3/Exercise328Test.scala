package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise325.{Branch, Leaf}
import org.scalatest.{FlatSpec, Matchers}

class Exercise328Test extends FlatSpec with Matchers {

  "size" should "count the number of nodes and leaves in a tree" in {
    Exercise328.map(Leaf(3))(_ + 1) should be(Leaf(4))
    Exercise328.map(Leaf(2))(_ + 1) should be(Leaf(3))
    Exercise328.map(Branch(Leaf(3), Branch(Leaf(1), Leaf(5))))(_ + 1) should be(Branch(Leaf(4), Branch(Leaf(2), Leaf(6))))
  }

}