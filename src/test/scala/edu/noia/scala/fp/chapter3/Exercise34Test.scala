package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise34Test extends FlatSpec with Matchers {

  import Exercise34._

  "drop" should "removes the first n elements from a list" in {
    drop(List(1, 2, 3), 0) should be(List(1, 2, 3))
    drop(List(1, 2, 3), 2) should be(List(3))
    drop(List(1, 2), 3) should be(Nil)
    drop(Nil, 1) should be(Nil)
  }
}