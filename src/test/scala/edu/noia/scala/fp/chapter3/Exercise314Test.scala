package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise314Test extends FlatSpec with Matchers {

  import Exercise314._

  "appendR" should "append a given list in front of another list" in {
    appendR(List(1, 2, 3), List(4, 5, 6)) should be(List(1, 2, 3, 4, 5, 6))
  }

  "appendL" should "append a given list in front of another list" in {
    appendL(List(1, 2, 3), List(4, 5, 6)) should be(List(1, 2, 3, 4, 5, 6))
  }
}