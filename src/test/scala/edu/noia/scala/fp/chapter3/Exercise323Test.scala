package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise323Test extends FlatSpec with Matchers {

  import Exercise323._

  "zip" should "add the content of two lists pairwise" in {
    zip(List(1, 2, 3), List(4, 5, 6))(_ + _) should be(List(5, 7, 9))
  }
}