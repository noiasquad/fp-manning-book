package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise316Test extends FlatSpec with Matchers {

  import Exercise316._

  "incL" should "transforms a list of integers by adding 1 to each element" in {
    incL(List(1, 2, 3), 1) should be(List(2, 3, 4))
  }

  "incR" should "transforms a list of integers by adding 1 to each element" in {
    incR(List(1, 2, 3), 1) should be(List(2, 3, 4))
  }
}