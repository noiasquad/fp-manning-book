package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise318Test extends FlatSpec with Matchers {

  import Exercise318._

  "mapL" should "modify each value in a list and maintain its structure" in {
    mapL(List(1, 2, 3))(_ + 1) should be(List(2, 3, 4))
    mapL(List(1.1, 0.1))(_.toString) should be(List("1.1", "0.1"))
  }

  "mapR" should "modify each value in a list and maintain its structure" in {
    mapR(List(1, 2, 3))(_ + 1) should be(List(2, 3, 4))
    mapR(List(1.1, 0.1))(_.toString) should be(List("1.1", "0.1"))
  }
}