package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise315Test extends FlatSpec with Matchers {

  import Exercise315._

  "concat" should "concatenate a list of lists into a single list" in {
    concat(List(List(1, 2, 3), List(4, 5, 6))) should be(List(1, 2, 3, 4, 5, 6))
  }
}