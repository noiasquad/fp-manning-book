package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise310Test extends FlatSpec with Matchers {

  import Exercise310._

  "foldLeft" should "sum the elements of the list" in {
    foldLeft(List(1, 2, 3), 0)(_ + _) should be(6)
    foldLeft(List(1, 2, 0), 0)(_ + _) should be(3)
    foldLeft(Nil: List[Int], 0)(_ + _) should be(0)
  }
}