package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise324Test extends FlatSpec with Matchers {

  import Exercise324._

  "startWith" should "find a sequence at the beginning of a given list" in {
    Exercise324.startWith(List(1, 2, 3), List(1)) should be(true)
    Exercise324.startWith(List(1, 2, 3), List(2)) should be(false)
    Exercise324.startWith(List(1, 2, 3), Nil) should be(true)
    Exercise324.startWith(Nil, Nil) should be(true)
    Exercise324.startWith(Nil, List(2)) should be(false)
  }

  "hasSubsequence" should "find a sequence in a given list" in {
    hasSubsequence(List(1, 2, 3), List(1)) should be(true)
    hasSubsequence(List(1, 2, 3), List(1, 2)) should be(true)
    hasSubsequence(List(1, 2, 3), List(2, 3)) should be(true)
    hasSubsequence(List(1, 2, 3), List(3)) should be(true)
    hasSubsequence(List(1, 2, 3), List(1, 3)) should be(false)
    hasSubsequence(List(1, 2, 3), List(4)) should be(false)
    hasSubsequence(List(1, 2, 3), Nil) should be(true)
    hasSubsequence(Nil, Nil) should be(true)
    hasSubsequence(Nil, List(4)) should be(false)
  }

}