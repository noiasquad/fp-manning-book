package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise319Test extends FlatSpec with Matchers {

  import Exercise319._

  "filterL" should "remove all odd numbers from a List[Int]" in {
    filterL(List(1, 2, 3, 5, 7, 4))(a => a % 2 == 0) should be(List(2, 4))
  }

  "filterR" should "remove all odd numbers from a List[Int]" in {
    filterR(List(1, 2, 3, 5, 7, 4))(a => a % 2 == 0) should be(List(2, 4))
  }
}