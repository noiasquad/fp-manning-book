package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise321Test extends FlatSpec with Matchers {

  import Exercise321._

  "filter" should "remove all odd numbers from a List[Int]" in {
    filter(List(1, 2, 3, 5, 7, 4))(a => a % 2 == 0) should be(List(2, 4))
  }
}