package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise317Test extends FlatSpec with Matchers {

  import Exercise317._

  "toStringL" should "turns each value in a list to a string" in {
    toStringL(List(1.1, 0.1)) should be(List("1.1", "0.1"))
  }

  "toStringR" should "turns each value in a list to a string" in {
    toStringR(List(1.1, 0.1)) should be(List("1.1", "0.1"))
  }
}