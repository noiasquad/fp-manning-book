package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise39Test extends FlatSpec with Matchers {

  "length" should "compute the length of a list" in {
    Exercise39.length(List(10, 11)) should be(2)
    Exercise39.length(Nil) should be(0)
  }
}