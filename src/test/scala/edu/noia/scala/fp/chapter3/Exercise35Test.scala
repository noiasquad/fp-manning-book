package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise35Test extends FlatSpec with Matchers {

  import Exercise35._

  val smallerThan10 = (x: Int) => x < 10

  "dropWhile" should "removes elements from the List prefix as long as they match a predicate" in {
    dropWhile(List(10, 11), (x: Int) => x < 10) should be(List(10, 11))
    dropWhile(List(9, 10), smallerThan10) should be(List(10))
    dropWhile(List(1, 2), smallerThan10) should be(Nil)
    dropWhile(Nil, smallerThan10) should be(Nil)
  }

  "dropWhileC" should "removes elements from the List prefix as long as they match a predicate" in {
    dropWhileC(List(10, 11))(x => x < 10) should be(List(10, 11))
    dropWhileC(List(9, 10))(x => x < 10) should be(List(10))
    dropWhileC(List(1, 2))(x => x < 10) should be(Nil)
    dropWhileC(Nil)((x: Int) => x < 10) should be(Nil)
  }
}