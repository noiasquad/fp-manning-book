package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise311Test extends FlatSpec with Matchers {

  import Exercise311._

  "length" should "compute the length of the list" in {
    Exercise311.length(List(1, 2, 3)) should be(3)
    Exercise311.length(Nil) should be(0)
  }

  "sum" should "sum the elements of the list" in {
    sum(List(1, 2, 3)) should be(6)
    sum(List(1, 2, 0)) should be(3)
    sum(Nil: List[Int]) should be(0)
  }

  "product" should "multiply the elements of the list" in {
    product(List(1, 2, 3)) should be(6)
    product(List(1, 2, 0)) should be(0)
    product(Nil: List[Int]) should be(0)
  }
}