package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise312Test extends FlatSpec with Matchers {

  import Exercise312._

  "reverse" should "reverse the content of the list" in {
    reverse(List(1, 2, 3)) should be(List(3, 2, 1))
    reverse(Nil) should be(Nil)
  }
}