package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise313Test extends FlatSpec with Matchers {

  import Exercise313._

  "foldLeftR" should "sum the elements of the list" in {
    foldLeftR(List(1, 2, 3), 0)(_ + _) should be(6)
    foldLeftR(List(1, 2, 0), 0)(_ + _) should be(3)
    foldLeftR(Nil: List[Int], 0)(_ + _) should be(0)
  }

  "foldRightL" should "sum the elements of the list" in {
    foldRightL(List(1, 2, 3), 0)(_ + _) should be(6)
    foldRightL(List(1, 2, 0), 0)(_ + _) should be(3)
    foldRightL(Nil: List[Int], 0)(_ + _) should be(0)
  }
}