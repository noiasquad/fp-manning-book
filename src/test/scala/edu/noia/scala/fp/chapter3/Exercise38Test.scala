package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

// What do you think this says about the relationship between foldRight and the data constructors of List?
// It says foldRight execution tree is similar to the List data structure built using Cons constructor
class Exercise38Test extends FlatSpec with Matchers {

  import Exercise38._

  "foldRight" should "build a copy of the list" in {
    foldRight(List(1, 2, 3), Nil: List[Int])(Cons(_, _)) should be(List(1, 2, 3))
  }
}