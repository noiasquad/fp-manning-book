package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise33Test extends FlatSpec with Matchers {

  import Exercise33._

  "setHead" should "replace the first element with a different value" in {
    setHead(List(1,2,3), 9) should be(List(9,2,3))
    setHead(Nil, 9) should be(List(9))
  }
}