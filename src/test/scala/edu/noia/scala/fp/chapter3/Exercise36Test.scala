package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise36Test extends FlatSpec with Matchers {

  import Exercise36._

  "init" should "return a List consisting of all but the last element of a List" in {
    init(List(10, 11)) should be(List(10))
    init(List(10)) should be(Nil)
    init(Nil) should be(Nil)
  }

  "initR" should "return a List consisting of all but the last element of a List" in {
    initR(List(10, 11)) should be(List(10))
    initR(List(10)) should be(Nil)
    initR(Nil) should be(Nil)
  }
}