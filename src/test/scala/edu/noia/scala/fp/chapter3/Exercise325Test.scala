package edu.noia.scala.fp.chapter3

import edu.noia.scala.fp.chapter3.Exercise325.{Branch, Leaf}
import org.scalatest.{FlatSpec, Matchers}

class Exercise325Test extends FlatSpec with Matchers {

  "size" should "count the number of nodes and leaves in a tree" in {
    Exercise325.size(Leaf(3)) should be(1)
    Exercise325.size(Branch(Leaf(3), Branch(Leaf(1), Leaf(5)))) should be(5)
  }

}