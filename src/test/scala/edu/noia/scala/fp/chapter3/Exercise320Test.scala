package edu.noia.scala.fp.chapter3

import org.scalatest.{FlatSpec, Matchers}

class Exercise320Test extends FlatSpec with Matchers {

  import Exercise320._

  "flatMapL" should "merge the inner lists into the original list" in {
    flatMapL(List(1, 2, 3))(i => List(i, i)) should be(List(1, 1, 2, 2, 3, 3))
  }

  "flatMapR" should "merge the inner lists into the original list" in {
    flatMapR(List(1, 2, 3))(i => List(i, i)) should be(List(1, 1, 2, 2, 3, 3))
  }
}