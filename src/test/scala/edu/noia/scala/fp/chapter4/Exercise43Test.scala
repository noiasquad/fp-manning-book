package edu.noia.scala.fp.chapter4

import org.scalatest.{FlatSpec, Matchers}
import Exercise41.{Some, None, Option}

class Exercise43Test extends FlatSpec with Matchers {

  import Exercise43._

  "map2 function" should "add two option values" in {
    map2(Some(1), Some(2))((a,b) => a+b) should be(Some(3))
    map2(Some(1), None)((a,b) => a+b) should be(None)
    map2(None: Option[Int], Some(2))((a,b) => a+b) should be(None)
  }
}