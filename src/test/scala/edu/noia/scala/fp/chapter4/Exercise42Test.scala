package edu.noia.scala.fp.chapter4

import org.scalatest.{FlatSpec, Matchers}
import Exercise41.{Some, None}

class Exercise42Test extends FlatSpec with Matchers {

  import Exercise42._

  "variance of empty list" should "return no value" in {
    variance(List()) should be(None)
  }

  "variance of list" should "return the mean of math.pow(x - m, 2) for each element in the list" in {
    variance(List(1)) should be(Some(0.0))
    variance(List(1, 1)) should be(Some(0.0))
    variance(List(1, 2)) should be(Some(math.pow(0.5, 2)))
  }
}