package edu.noia.scala.fp.chapter4

import edu.noia.scala.fp.chapter4.Exercise41.{None, Option, Some}
import org.scalatest.{FlatSpec, Matchers}

class Exercise44Test extends FlatSpec with Matchers {

  import Exercise44._

  "sequence function" should "combine a list of options into one option" in {
    sequence(List(Some(1), Some(2))) should be(Some(List(1, 2)))
    sequence(List(Some(1), None)) should be(None)
    sequence(List(None: Option[Int], Some(2))) should be(None)
  }
}