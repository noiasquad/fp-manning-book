package edu.noia.scala.fp.chapter4

import edu.noia.scala.fp.chapter4.Exercise41.{Some}
import org.scalatest.{FlatSpec, Matchers}

class Exercise45Test extends FlatSpec with Matchers {

  import Exercise45._

  "parseInts functions" should "return a list of integers from a list of strings" in {
    parseInts(List("1", "2", "3")) should be(Some(List(1, 2, 3)))
    parseInts1(List("1", "2", "3")) should be(Some(List(1, 2, 3)))
    parseInts2(List("1", "2", "3")) should be(Some(List(1, 2, 3)))
  }
}