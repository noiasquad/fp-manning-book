package edu.noia.scala.fp.chapter4

import edu.noia.scala.fp.chapter4.Exercise41.{None, Some}
import org.scalatest.{FlatSpec, Matchers}

class Exercise41Test extends FlatSpec with Matchers {

  "map function" should "transform the value contained" in {
    Some(1) map (_ + 1) should be(Some(2))
  }

  "flatmap function" should "transform the value contained and maybe fails" in {
    Some(1) flatMap (x => Some(x + 1)) should be(Some(2))
  }

  "getOrElse function" should "return the value contained if any else a given default value" in {
    Some(1) getOrElse 0 should be(1)
    None getOrElse 0 should be(0)
  }

  "orElse function" should "return some value contained if any or an given optional value" in {
    Some(1) orElse Some(0) should be(Some(1))
    None getOrElse Some(0) should be(Some(0))
  }

  "filter function" should "return some value contained which satisfies a given predicate" in {
    Some(2) filter (x => x % 2 == 0) should be(Some(2))
    Some(1) filter (x => x % 2 == 0) should be(None)
    None filter (_ => true) should be(None)
  }

}